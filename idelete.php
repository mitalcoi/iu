#!/usr/bin/env php
<?php
require_once(dirname(__FILE__) . '/vendor/autoload.php');
$worker = new \GearmanWorker();
$worker->addServer('127.0.0.1', 4730);
$worker->addFunction("delete", "gm_delete");
while (1) {
    echo "wait for work\n";
    $worker->work();
    if ($worker->returnCode() != GEARMAN_SUCCESS){
    echo "return_code: " . $worker->returnCode() . "\n";
    break;
  }
}

function gm_delete($job) {
    $imageTransport = new \RTransport\Image\Server;
    $imageTransport->setRelativePath(dirname(__FILE__) . '/web/');
    echo $imageTransport->delete($job->workload());
}

