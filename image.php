#!/usr/bin/env php
<?php
require_once(dirname(__FILE__) . '/vendor/autoload.php');
$worker = new GearmanWorker();
$worker->addServer('127.0.0.1', 4730);
$worker->addFunction("upload", "upload");
$worker->addFunction("delete", "delete");
while (1) {
    $worker->work();
    if ($worker->returnCode() != GEARMAN_SUCCESS)
        break;
}

function upload(GearmanJob $job) {
    $imageTransport = new RTransport\Image\Server;
    $imageTransport->setRelativePath(dirname(__FILE__) . '/web/');
    $imageTransport->setUploadDomen('/');
    echo $imageTransport->upload($job->workload());
}

function delete(GearmanJob $job) {
    $imageTransport = new RTransport\Image\Server;
    $imageTransport->setRelativePath(dirname(__FILE__) . '/web/');
    $imageTransport->setUploadDomen('/');
    echo $imageTransport->delete($job->workload());
}

