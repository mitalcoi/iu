<?php
return array(
    'basePath' => dirname(__FILE__) . '/../protected/',
    'language' => 'ru',
    'charset' => 'UTF-8',
    'import' => array(
        'application.components.*',
    ),
    'urlManager' => array(
            'class' => 'CUrlManager',
            'urlFormat' => 'path',
            'showScriptName' => false,
    ),
);

