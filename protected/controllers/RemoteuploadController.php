<?php

class RemoteuploadController extends Controller {
    
    public function actionClearAll(){
        if(YII_DEBUG){
            $this->dd(Yii::getPathOfAlias('images'));
        }
    }
    public function actionTouch(){
        if(YII_DEBUG){
            $file = 'test.jpg';
            $hash = md5($file);
            $path = $this->createPath($hash);
            system('touch '.$path.$hash.'.jpg');
            $this->sendResponse(200, Yii::app()->name .'/'.$path.$hash.'.jpg');
        }
    }
   
}
