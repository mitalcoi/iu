<?php
date_default_timezone_set('Europe/Kiev');
define('LOCAL_INSTANCE', dirname(__FILE__) === '/var/www/iu/protected' ? false : true);
defined('YII_DEBUG') or define('YII_DEBUG', true);
$config = dirname(__FILE__) . '/../config/console.php';
$yiic = dirname(__FILE__) . '/../vendor/yiisoft/yii/framework/yiic.php';
require_once(dirname(__FILE__).'/../vendor/autoload.php');
require_once($yiic);

Yii::createConsoleApplication($config);
