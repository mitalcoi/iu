<?php

/**
 * Main class for managing workers across yiic
 * All public function with prefix 'worker' should be correct workers
 */
class WorkersCommand extends CConsoleCommand {

    /**
     * Set worker
     * @param string $name name of worker action
     */
    public function actionSet($name) {
        if (!method_exists($this, 'worker' . ucfirst($name)))
            die("No defined worker\n");
        $worker = Yii::app()->gearman->worker();
        $worker->addFunction($name, array($this, 'worker' . ucfirst($name)));
        while ($worker->work()) {
            if ($worker->returnCode() != GEARMAN_SUCCESS) {
                break;
            }
        }
    }

    /**
     * Unset worker
     * @param string $name name of worker action.
     */
    public function actionUnset($name) {
        if (!method_exists($this, 'worker' . ucfirst($name)))
            die("No defined worker\n");
        Yii::app()->gearman->client()->doHighBackground($name, CJSON::encode(array()));
        $worker = Yii::app()->gearman->worker();
        $worker->addFunction($name, array($this, 'resetChain'));
        while ($worker->work()) {
            if ($worker->returnCode() != GEARMAN_SUCCESS)
                break;
        }
    }

    /**
     * 
     * @param GearmanJob $job contain: bytes, hash. thumbs, prefix
     */
    public function workerImageUpload(GearmanJob $job) {
        $data = CJSON::decode($job->workload());
        if (!isset($data['bytes']) || !isset($data['hash']) || !isset($data['thumbs']) || !isset($data['prefix'])) {
            echo 'wrong workload' . print_r($data, true) . "\n";
        } else {
            $bytes = utf8_decode($data['bytes']);
            $hash = $data['hash'];
            $filename = $hash . '.jpg';
            $path = $this->getRelativePath() . 'iu/' . $data['prefix'] . '/' . $hash[0] . '/' . $hash[1] . '/' . $hash[2] . '/';
            if (!file_exists($path))
                mkdir($path, 0777, true);
            $file = fopen($path . $filename, 'w');
            fwrite($file, $bytes);
            fclose($file);
            $image = Yii::app()->image->load($path . $filename)->save($path . $filename, CImageHandler::IMG_JPEG)->reload();
            echo "file written: " . $path . $filename . "\n";
            if ($data['thumbs']) {
                foreach ($data['thumbs'] as $thumb) {
                    if ($thumb) {
                        preg_match('/_([0-9]+)x/', $thumb, $mathes);
                        if (isset($mathes[1])) {
                            $size = $mathes[1];
                            $image->thumb($size, $size)->save($path . $filename . $thumb, CImageHandler::IMG_JPEG)->reload();
                        }
                    }
                }
            }
        }
    }

    public function workerImageDelete(GearmanJob $job) {
        $data = CJSON::decode($job->workload());
        if (!isset($data['path'])) {
            echo 'wrong workload:' . print_r($data, true) . "\n";
        } else {
            if (!$data['path']) {
                echo "empty path!\n";
            } else {
                $path = str_replace(Yii::app()->name, '', $data['path']);
                $path = $this->getRelativePath() . $path;
                foreach (glob($path . '*') as $del) {
                    unlink($del);
                    echo "file deleted: " . $del . "\n";
                }
            }
        }
    }

    private function getWatermark() {
        return $this->getRelativePath() . 'watermarks/watermark.png';
    }

    private function getRelativePath() {
        return dirname(__FILE__) . '/../../web/';
    }

    /**
     * Internal function which reset all job for certain workers
     * @param GearmanJob $job
     */
    public function resetChain(GearmanJob $job) {
        echo "unset\n";
    }

}

