<?php

class Gearman extends CApplicationComponent
{

    public $servers;
    protected $_client;
    protected $_worker;

    public function init()
    {
        parent::init();
    }

    protected function setServers($instance)
    {
        foreach ($this->servers as $s)
        {
            $instance->addServer();
        }
        return $instance;
    }

    public function client()
    {
        if (!$this->_client)
        {
            $this->_client = $this->setServers(new GearmanClient());
        }

        return $this->_client;
    }

    public function worker()
    {
        if (!$this->_worker)
        {
            $this->_worker = $this->setServers(new GearmanWorker());
        }

        return $this->_worker;
    }

}
