<?php

Yii::setPathOfAlias('images', dirname(__FILE__) . '/../web/iu/');
return array(
    'basePath' => dirname(__FILE__) . '/../protected/',
    'language' => 'ru',
    'charset' => 'UTF-8',
    'import' => array(
        'application.components.*',
    ),
    'components' => array(
        'gearman' => array(
            'class' => 'Gearman',
            'servers' => array(
                array('host' => '127.0.0.1', 'port' => 4730),
            ),
        ),
        'image' => array(
            'class' => 'ext.image.CImageHandler',
        ),
    ),
);

