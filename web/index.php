<?
define('LOCATION', isset($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] === 'production' ? 'production' : 'local');
date_default_timezone_set('Europe/Kiev'); 
if (LOCATION == 'local') {
    $config = dirname(__FILE__) . '/../config/local.php';
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
} else {
    defined('YII_DEBUG') or define('YII_DEBUG', false);
    $config = dirname(__FILE__) . '/../config/remote.php';
}
$config = dirname(__FILE__) . '/../config/main.php';
$yii = dirname(__FILE__) . '/../vendor/yiisoft/yii/framework/yii.php';
require_once($yii);
Yii::createWebApplication($config)->run();
